import React from 'react'
import './App.css';
import Game from './Components/Game'


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>TIC TAC TOE</h1>
      </header>
      <Game />
    </div>
  );
}

export default App;
